mod input;

use colored::Colorize;
use std::env;
use std::fs::File;
use std::io::Read;
use std::process::Command;
use input::input;

const SPLIT: &str = "$%&*^&!$$";
const OS: &str = env::consts::OS;

fn main() {
    let command_line: Vec<String> = env::args().collect();
    let file_name: &str;

    let mut file_index: usize = 0;

    let mode: &str;
    let mut code_mode = String::from("null");

    if command_line.len() == 1 {
        mode = "live";
        file_name = "null";
    } else {
        mode = "file";
        file_name = command_line[1].as_str();
    }

    loop {
        let mut code_line: String;
        
        if mode == "file" {
            let file_exists = match File::open(file_name) {
                Ok(_o) => true,
                Err(_e) => false
            };

            if file_exists == false {
                error("The file you have specified does not exist!");
                break;
            } else {
                if file_name.ends_with(".scl") == false {
                    error("The file you have specified is not an SCL file!");
                    break;
                }
            }

            let mut file = File::open(file_name).unwrap();
            let mut data_raw = String::new();
            file.read_to_string(&mut data_raw).unwrap();
            let data = read_to_vectored_string(&data_raw);

            if data.len() > file_index {
                code_line = data[file_index].to_string();
            } else {
                break;
            }

            file_index += 1;
        } else {
            code_line = input("Demo Shell ->".cyan().italic().to_string().as_str(), true);
        }

        //// Cutter ////
        let code_line_raw = &code_line.to_string();

        // Stage 1: Commenting
        if code_line.starts_with("#") {
            code_line = String::from("");
        }

        // Stage 2: Strings
        let mut cut = true;
        let mut phrase = String::new();
        for i in code_line.chars() {
            if i == '\"' {
                cut = !cut;
            }

            if cut == true {
                if i == ' ' {
                    phrase.push_str(SPLIT);
                } else {
                    if i != '"' {
                        phrase.push(i);
                    }
                }
            } else {
                if i != '"' {
                    phrase.push(i);
                }
            }
        }

        // Stage 3: Turning "\'" into """ that the user can use!
        let mut final_phrase = String::new();
        let mut vec_final_phrase: Vec<String> = Vec::new();
        for i in phrase.split("\\'") {
            vec_final_phrase.push(i.to_string());
        }
        for i in 0..vec_final_phrase.len() {
            final_phrase.push_str(vec_final_phrase[i].as_str());
            if i != vec_final_phrase.len() - 1 {
                final_phrase.push_str("\"");
            }
        }

        // Stage 4: Splitting into the code vector!
        let mut code: Vec<String> = Vec::new();
        for i in final_phrase.split(SPLIT) {
            code.push(i.to_string());
        }

        //// Interpreter ////
        if code_line.starts_with("[") && code_line.ends_with("]") {
            if code_line == "[code]" {
                code_mode = String::from("code");
            }

            else if code_line == "[echo]" {
                code_mode = String::from("echo");
            }

            else {
                code_mode = String::from("error");
            }
        }

        if code_mode == "code" {
            if code_line == "[code]" {
                //code_line = String::new();
                code = vec![String::new()];
            }

            if code[0] == "print" {
                if code.len() != 2 {
                    line_error(file_index, "Takes only one argument!");
                } else {
                    println!("{}", code[1]);
                }
            }

            else if code[0] == "printc" {
                if code.len() != 2 {
                    line_error(file_index, "Takes only one argument!");
                } else {
                    print!("{}", code[1]);
                }
            }

            else if code[0] == "exit" {
                if code.len() != 1 {
                    line_error(file_index, "Takes no arguments!");
                } else {
                    break;
                }
            }

            else if code[0] == "call-command" {
                if code.len() != 2 {
                    line_error(file_index, "Takes only one argument!");
                } else {
                    if OS != "windows" {
                        Command::new("bash").args(["-c", code[1].as_str()]).status().unwrap();
                    } else {
                        Command::new("cmd").args(["/C", code[1].as_str()]).status().unwrap();
                    }
                }
            }

            else if code[0] == "call-hidden-command" {
                if code.len() != 2 {
                    line_error(file_index, "Takes only one argument!");
                } else {
                    if OS != "windows" {
                        Command::new("bash").args(["-c", code[1].as_str()]).output().unwrap();
                    } else {
                        Command::new("cmd").args(["/C", code[1].as_str()]).output().unwrap();
                    }
                }
            }

            else if code[0] == "" {
                // Pass.
            }

            else {
                line_error(file_index, "That is not a valid line of code!");
            }
        }

        else if code_mode == "echo" {
            if code_line_raw != "[echo]" {
                println!("{}", code_line_raw);
            }
        }

        else if code_mode == "null" {
            if code_line != "" {
                error("Please specify a mode!");
            }
        }

        else if code_mode == "error" {
            line_error(file_index, "Not a valid mode!");
            break;
        }

        else {
            error("No mode specified!");
            break;
        }
    }
}

fn line_error(line: usize, error: &str) {
    let line_symbol: String;
    if line == 0 {
        line_symbol = String::from("*");
    } else {
        line_symbol = line.to_string();
    }
    println!("{} {} {}", format!("[line: {}]", line_symbol).truecolor(255, 155, 155), "->".truecolor(85, 85, 85), error.truecolor(205, 105, 105));
}

fn error(error: &str) {
    println!("{} {} {}", "[SCL Error]".truecolor(255, 155, 155), "->".truecolor(85, 85, 85), error.truecolor(205, 105, 105));
}

fn read_to_vectored_string(text: &String) -> Vec<String> {
    let mut return_vec: Vec<String> = Vec::new();

    for i in text.split("\n") {
        return_vec.push(i.to_string());
    }

    return return_vec;
}
